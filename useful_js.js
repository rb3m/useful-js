// Position the footer of a site flush against the bottom of the window if document shorter.
// For those times when you can't use position: fixed. Requires jQuery because I'm lazy.
function footertoBottom() {
    var bHeight = jQuery(document).height();
    var fPosition = jQuery("footer").offset();
    var fHeight = jQuery("footer").outerHeight();
    if ((fPosition.top + fHeight) < bHeight) {
        var newTop = bHeight - fHeight;
        jQuery("footer").offset({ top: newTop, left: fPosition.left});
    }
    
}